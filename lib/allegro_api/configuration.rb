module AllegroApi
  class Configuration

    attr_accessor :api_key, :login, :password, :country_code
  end
end
